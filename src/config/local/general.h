#define	DOWNLOAD_PROTO_HTTPS	/* Secure Hypertext Transfer Protocol */

#define	DNS_RESOLVER		/* DNS resolver */

#define VLAN_CMD		/* VLAN commands */
#define REBOOT_CMD		/* Reboot command */
#define POWEROFF_CMD		/* Power off command */
#define PING_CMD		/* Ping command */
#define CONSOLE_CMD		/* Console command */

#define	IMAGE_PNG		/* PNG image support */
