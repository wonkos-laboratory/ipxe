#!/bin/bash

SCRIPT_DIR="${0%/*}"
BUILD_DIR="$SCRIPT_DIR/build"
if [ ! -d $BUILD_DIR/.git ]; then
  rm -r $BUILD_DIR
  git clone git://git.ipxe.org/ipxe.git $BUILD_DIR
fi 

cp -rv $SCRIPT_DIR/src $BUILD_DIR/
pushd $BUILD_DIR/src
make bin/undionly.kpxe EMBED=speedy.script 
popd
